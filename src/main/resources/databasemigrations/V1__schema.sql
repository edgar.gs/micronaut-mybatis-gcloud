
CREATE TABLE CLIENT (
  id    BIGINT SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255)              NOT NULL,
  lastname VARCHAR(255)              NOT NULL,
  age INT,
  birth_date DATE,
  predicted_death_date DATE
);
