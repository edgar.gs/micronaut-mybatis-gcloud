package example.micronaut.dto;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotNull;

import java.sql.Date;

@Introspected
public class KPIClient {

    public KPIClient() {}

    private double promedio;
    private double desviacion;

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public double getDesviacion() {
        return desviacion;
    }

    public void setDesviacion(double desviacion) {
        this.desviacion = desviacion;
    }

    @Override
    public String toString() {
        return "KPIClient{" +
                "promedio=" + promedio +
                ", desviacion='" + desviacion + '\'' +
                '}';
    }
}
