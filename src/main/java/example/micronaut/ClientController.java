package example.micronaut;

import example.micronaut.domain.Client;
import example.micronaut.dto.KPIClient;
import example.micronaut.client.ClientRepository;
import example.micronaut.client.ClientSaveCommand;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@Controller("/") // <1>
public class ClientController {

    protected final ClientRepository clientRepository;

    public ClientController(ClientRepository clientRepository) { // <2>
        this.clientRepository = clientRepository;
    }

    @Get("/clients/{id}") // <3>
    public Client show(Long id) {
        return clientRepository
                .findById(id)
                .orElse(null); // <4>
    }

    @Get(value = "/listclientes{?args*}") // <8>
    public List<Client> list(@Valid ListingArguments args) {
        return clientRepository.findAll(args);
    }

     @Get(value = "/kpideclientes")
    public KPIClient kpideclientes() {
        double promedio = 0;
        double varianza = 0;
        double desviacion = 0;

        /*
        https://es.khanacademy.org/math/probability/data-distributions-a1/summarizing-spread-distributions/a/calculating-standard-deviation-step-by-step
        https://platzi.com/tutoriales/1352-ia/1314-calcular-varianza-y-desviacion-estandar-de-cualquier-lista-de-datos/
        Esta formula se explica mejor siguiendo estos pasos:
        a) Calcular la suma de los datos.
        b) Calcular la media.
        c) Restar la media a cada dato.
        d) Elevar al cuadrado cada resultado.
        e) Sumar todos los resultados y sacar la media de esta sumatoria.
        f) Raíz cuadrada del resultado.
        */

        List<Integer> lstEdad;
        //TODO Test with Spock
        //lstEdad = Arrays.asList(490, 500, 510, 515, 520);
        //lstEdad = Arrays.asList(2, 3, 6, 8, 11);
        //lstEdad = Arrays.asList(12, 6, 7, 3, 15, 10, 18, 5);
        lstEdad = clientRepository.getListAge();

        System.out.println(lstEdad);
        IntSummaryStatistics stats = lstEdad.stream()
                                     .mapToInt((x) -> x)
                                     .summaryStatistics();
        System.out.println(stats);
        
        promedio = stats.getAverage(); //b
        System.out.println("promedio="+promedio);

        List<Double> auxEdad = new ArrayList<>();
        for(Integer edad : lstEdad){
            auxEdad.add(Math.pow(edad-promedio,2)); //c, d
        }

        System.out.println(auxEdad);
        DoubleSummaryStatistics statsD = auxEdad.stream()
                        .mapToDouble((x) -> x)
                        .summaryStatistics();
        System.out.println(statsD);
        
        varianza = statsD.getAverage(); //e
        System.out.println("varianza="+varianza);

        desviacion = Math.sqrt(varianza); //f
        System.out.println("desviacion="+desviacion);

        KPIClient kpiCliente = new KPIClient();
        kpiCliente.setPromedio(promedio);
        kpiCliente.setDesviacion(desviacion);

        return kpiCliente;
    }

    @Post("/creacliente") // <9>
    public HttpResponse<Client> save(@Body @Valid ClientSaveCommand cmd) {
        Client client = clientRepository.save(cmd.getName(), cmd.getLastname(), cmd.getAge(), cmd.getBirth_date());

        return HttpResponse
                .created(client)
                .headers(headers -> headers.location(location(client.getId())));
    }

    @Delete("/clients/{id}") // <10>
    public HttpResponse delete(Long id) {
        clientRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    protected URI location(Long id) {
        return URI.create("/clients/" + id);
    }
}
