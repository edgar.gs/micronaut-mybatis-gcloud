package example.micronaut.client;

import example.micronaut.ListingArguments;
import example.micronaut.domain.Client;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import java.sql.Date;

public interface ClientRepository {

    Optional<Client> findById(@NotNull Long id);

    Client save(@NotBlank String name,
                        @NotBlank String lastname,
                        @NotNull int age,
                        @NotNull Date birth_date);

    void deleteById(@NotNull Long id);

    List<Client> findAll(@NotNull ListingArguments args);

    List<Integer> getListAge();
}
