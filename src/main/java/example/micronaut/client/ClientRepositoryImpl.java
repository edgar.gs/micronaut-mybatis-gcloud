package example.micronaut.client;

import example.micronaut.ListingArguments;
import example.micronaut.domain.Client;

import javax.inject.Singleton;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import java.sql.Date;

@Singleton // <1>
public class ClientRepositoryImpl implements ClientRepository {

    private final ClientMapper clientMapper;

    public ClientRepositoryImpl(ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }

    @Override
    public Optional<Client> findById(@NotNull Long id) {
        return Optional.ofNullable(clientMapper.findById(id));
    }

    @Override
    public Client save(@NotBlank String name,
                        @NotBlank String lastname,
                        @NotNull int age,
                        @NotNull Date birth_date) {
        Client client = new Client(name, lastname, age, birth_date);

        //Predicted Death Date
        int years = 70;
        java.time.LocalDate auxDate = birth_date.toLocalDate();
        auxDate = auxDate.plus(java.time.Period.ofYears(years));
        client.setPredicted_death_date(Date.valueOf(auxDate));
                
        clientMapper.save(client);
        return client;
    }

    @Override
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(client -> clientMapper.deleteById(id));
    }

    public List<Client> findAll(@NotNull ListingArguments args) {

        return clientMapper.findAll();
    }

    @Override
    public List<Integer> getListAge(){
        return clientMapper.getListAge();
    }
}
