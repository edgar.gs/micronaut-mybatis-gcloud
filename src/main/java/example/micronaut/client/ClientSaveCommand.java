package example.micronaut.client;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.sql.Date;

@Introspected
public class ClientSaveCommand {

    @NotBlank
    private String name;

    @NotBlank
    private String lastname;

    @NotNull
    private int age;

    @NotNull
    private Date birth_date;

    public ClientSaveCommand() {
    }

    public ClientSaveCommand(String name, String lastname, int age, Date birth_date) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.birth_date = birth_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }
}
