package example.micronaut.client;

import example.micronaut.domain.Client;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Singleton // <1>
public class ClientMapperImpl implements ClientMapper {

    private final SqlSessionFactory sqlSessionFactory; // <2>

    public ClientMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory; // <2>
    }

    @Override
    public Client findById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) { // <3>
            return getClientMapper(sqlSession).findById(id); // <5>
        }
    }

    private ClientMapper getClientMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ClientMapper.class); // <4>
    }


    @Override
    public void save(Client client) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getClientMapper(sqlSession).save(client);
            sqlSession.commit(); // <6>
        }
    }

    @Override
    public void deleteById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getClientMapper(sqlSession).deleteById(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Client> findAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getClientMapper(sqlSession).findAll();
        }
    }

    @Override
    public List<Integer> getListAge() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getClientMapper(sqlSession).getListAge();
        }
    }
}
