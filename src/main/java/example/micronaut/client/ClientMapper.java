package example.micronaut.client;

import example.micronaut.domain.Client;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

public interface ClientMapper {

    @Select("select * from client where id=#{id}")
    Client findById(long id);

    @Insert("insert into client(name, lastname, age, birth_date, predicted_death_date) values(#{name}, #{lastname}, #{age}, #{birth_date}, #{predicted_death_date})")
    @Options(useGeneratedKeys = true)
    void save(Client client);

    @Delete("delete from client where id=#{id}")
    void deleteById(long id);

    @Select("select * from client")
    List<Client> findAll();

    @Select("select age from client")
    List<Integer> getListAge();

}
