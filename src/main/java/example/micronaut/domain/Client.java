package example.micronaut.domain;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotNull;

import java.text.SimpleDateFormat;
import java.sql.Date;

@Introspected
public class Client {

    //TODO Implements with DTO
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public Client() {}

    public Client(@NotNull String name,
                  @NotNull String lastname,
                  @NotNull int age,
                  @NotNull Date birth_date){
        this.setName(name);
        this.setLastname(lastname);
        this.setAge(age);
        this.setBirth_date(birth_date);
    }

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String lastname;

    @NotNull
    private int age;

    @NotNull
    private Date birth_date;

    private Date predicted_death_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getBirth_date_str() {
        return sdf.format(birth_date);
    }

    public Date getPredicted_death_date() {
        return predicted_death_date;
    }

    public void setPredicted_death_date(Date predicted_death_date) {
        this.predicted_death_date = predicted_death_date;
    }

    public String getPredicted_death_date_str() {
        return sdf.format(predicted_death_date);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }
}
