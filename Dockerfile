
FROM gradle:5.3-jdk8-alpine as builder
WORKDIR /app
COPY build.gradle .
COPY gradle.properties .
COPY src ./src
USER root
RUN gradle build -x test


FROM openjdk:8u171-alpine3.7
COPY --from=builder /app/build/libs/*-all.jar complete.jar
CMD java  -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar complete.jar
